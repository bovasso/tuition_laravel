<?php namespace Controllers\Api;

use Restable;
use Input;
use Lang;
use Goal;
use Redirect;
use Sentry;
use Str;
use Validator;
use Session;
use Facebook;

class AuthController extends \BaseController {

	/**
	 * Account sign in.
	 *
	 * @return View
	 */
	public function status() {
		// Show the page
		return Response::json(Sentry::check());
	}


	public function logout() {
		Sentry::logout();
    	return Response::json(array('flash' => 'Logged Out!'));
	}

	/**
	 * Facebook Connect
	 *
	 * @return void
	 * @author Anthony Bovasso
	 * */
	public function authenticate() {
		// See if there is a user from a cookie
		$fb = 684621679;

		if ($fb) {
			try {
				//check for existing user by fb_uid
				$user = User::where('provider_uid', '=', $fb);

				// create user if one doesn't exist
				if (!$user) {
					// add user
					$user_profile        = $this->fb->api('/me');
					$birthday            = $user_profile['birthday'];
					$dob                 = date("Y-m-d",strtotime($birthday));
					$user                = new User();
					$user->first_name    = $user_profile['first_name'];
					$user->last_name     = $user_profile['last_name'];
					$user->username    	 = $user_profile['username'];
					$user->hometown      = $user_profile['hometown']['name'];
					$user->email         = $user_profile['email'];
					$user->dob           = $dob;
					$user->gender        = $user_profile['gender'];
					$user->provider      = 'facebook';
					$user->provider_uid  = $user_profile['id'];
					$user->access_token  = $this->fb->getExtendedAccessToken();
					$user->is_active     = 1;
					$user->touch();
					$user->save();

				} else {
					// update fields if needed
					$user_profile = $this->fb->api('/me');
					$birthday = $user_profile['birthday'];
					$user->provider_uid = $user_profile['id'];
					$user->hometown      = $user_profile['hometown']['name'];
					$user->first_name    = $user_profile['first_name'];
					$user->last_name     = $user_profile['last_name'];
					$user->email         = $user_profile['email'];
					$user->gender        = $user_profile['gender'];
					// turn fb string birthday to date
					$dob = date("Y-m-d",strtotime($birthday));
					//return a list of user's colleges
					$user->dob = $dob;
					$user->access_token = $this->fb->getExtendedAccessToken();
					$user->touch();
					$user->save();
				}

				// store user's data in session
				Session::put('id', $user->id);
				Session::put('email', $user->email);
				Session::put('provider_uid', $user->provider_uid);
				Session::put('access_token', $this->fb->getExtendedAccessToken());
				Session::put('friends', $this->fb->getFriendIDs());

				return Response::json(Sentry::getUser());

			} catch (FacebookApiException $e) {
				//echo '<pre>' . htmlspecialchars(print_r($e, true)) . '</pre>';
				$user = null;
			}
		} else {
    		return Response::json(array('flash' => 'There was an error connecting to FB'));
			exit;
		}
	}

}
/* End of file register.php */
