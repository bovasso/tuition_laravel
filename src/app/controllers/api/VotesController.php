<?php

class VotesController extends \BaseController {

	/**
	 * Display a listing of votes
	 *
	 * @return Response
	 */
	public function index()
	{
		$votes = Vote::all();

		return View::make('votes.index', compact('votes'));
	}

	/**
	 * Show the form for creating a new vote
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('votes.create');
	}

	/**
	 * Store a newly created vote in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Vote::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Vote::create($data);

		return Redirect::route('votes.index');
	}

	/**
	 * Display the specified vote.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$vote = Vote::findOrFail($id);

		return View::make('votes.show', compact('vote'));
	}

	/**
	 * Show the form for editing the specified vote.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$vote = Vote::find($id);

		return View::make('votes.edit', compact('vote'));
	}

	/**
	 * Update the specified vote in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$vote = Vote::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Vote::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$vote->update($data);

		return Redirect::route('votes.index');
	}

	/**
	 * Remove the specified vote from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Vote::destroy($id);

		return Redirect::route('votes.index');
	}

}
