<?php namespace Controllers\Api;

use Restable;
use Input;
use Lang;
use Goal;
use Redirect;
use Sentry;
use Str;
use Validator;
use Session;

class GoalsController extends \BaseController {

	/**
	 * Display a listing
	 *
	 * @return Response
	 */
	public function index($user_id = null) {

        if (!is_null($user_id)){
            $goals = Goal::compact()->where('user_id', '=', $user_id)->get();
        } else{
            $goals = Goal::compact()->orderBy('created_at', 'DESC')->paginate(12);
        }

		if (!$goals){
			return Restable::missing()->render();
		}

		return Restable::listing($goals)->render('json');
	}

	/**
	 * Display a listing
	 *
	 * @return Response
	 */
	public function leaderboard() {

		$goals = Goal::compact()->orderBy('goals.total_votes', 'DESC' )->paginate(12);

		if (!$goals){
			return Restable::missing()->render();
		}

		return Restable::listing($goals)->render('json');
	}

	/**
     * Display the specified resource.
     *
     * @param  int  $user_id
     * @return Response
     */
    public function show($user_id) {

		$goal = Goal::compact()->where('user_id', '=', $user_id)->get();

		if (!$goal){
			return Restable::missing()->render();
		}

		return Restable::single($goal)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return View::make('api.goals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
        $goal = new Goal;

        $validator = Validator::make(Input::all(), Config::get('validation.api.goal'));

        if ($validator->fails()) {
            return Restable::unprocess($validator)->render();
        }

        $goal->headline = Input::get('headline');
        $goal->body = Input::get('body');
        $goal->touch();
        $goal->save();

        return Restable::created($goal)->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $goal = Goal::find($id);

        if (!$goal) {
            return Restable::missing()->render();
        }

        return View::make('api.goals.edit', compact('goal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        $goal = Goal::find($id);

        if (!$goal) {
            return Restable::missing()->render();
        }

        $validator = Validator::make(Input::all(), Config::get('validation.api.goal'));

        if ($validator->fails())
        {
            return Restable::unprocess($validator)->render();
        }

        $goal->headline = Input::get('title');
        $goal->body = Input::get('description');
        $goal->touch();
        $goal->save();

        return Restable::updated($goal)->render();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $goal = Goal::find($id);

        if (!$goal) {
            return Restable::missing()->render();
        }

        $goal->delete();

        return Restable::deleted()->render();
    }

}
