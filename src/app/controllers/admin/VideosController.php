<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Video;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;

class VideosController extends AdminController {

	/**
	 * Show a list of all the blog video.
	 *
	 * @return View
	 */
	public function index()
	{
		// Grab all the blog video
		$videos = Video::orderBy('created_at', 'DESC')->paginate(10);

		// Show the page
		return View::make('backend/videos/index', compact('videos'));
	}

	/**
	 * Blog video create.
	 *
	 * @return View
	 */
	public function create()
	{
		// Show the page
		return View::make('backend/videos/create');
	}

	/**
	 * Blog video create form processing.
	 *
	 * @return Redirect
	 */
	public function store()
	{
		// Declare the rules for the form validation
		$rules = array(
			'youtube_id'   => 'required|min:3',
			'janrain_id' => 'required|min:3',
			'provider_uid' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Create a new blog video
		$video = new video;

		// Update the blog video data
		$video->youtube_id         = e(Input::get('youtube_id'));
		$video->janrain_id          = e(Input::get('janrain_id'));
		$video->provider_uid          = Sentry::getUser()->provider_uid;

		// Was the blog video created?
		if($video->save())
		{
			// Redirect to the new blog video page
			return Redirect::to("admin/videos/$video->id/edit")->with('success', Lang::get('admin/videos/message.create.success'));
		}

		// Redirect to the blog video create page
		return Redirect::to('admin/videos/create')->with('error', Lang::get('admin/videos/message.create.error'));
	}

	/**
	 * Blog video update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function edit($id = null)
	{
		// Check if the blog video exists
		if (is_null($video = Video::find($id)))
		{
			// Redirect to the videos management page
			return Redirect::to('admin/videos')->with('error', Lang::get('admin/videos/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/videos/edit', compact('video'));
	}

	/**
	 * Blog video update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function update($id = null)
	{
		// Check if the blog video exists
		if (is_null($video = Video::find($id)))
		{
			// Redirect to the videos management page
			return Redirect::to('admin/videos')->with('error', Lang::get('admin/videos/message.does_not_exist'));
		}

		// Declare the rules for the form validation
		$rules = array(
			'headline'   => 'required|min:3',
			'body' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Update the blog video data
		$video->headline         = e(Input::get('headline'));
		$video->body          = e(Input::get('body'));
		$video->user_id          = Sentry::getUser()->provider_uid;

		// Was the blog video updated?
		if($video->save())
		{
			// Redirect to the new blog video page
			return Redirect::to("admin/videos/$id/edit")->with('success', Lang::get('admin/videos/message.update.success'));
		}

		// Redirect to the videos video management page
		return Redirect::to("admin/videos/$id/edit")->with('error', Lang::get('admin/videos/message.update.error'));
	}

	/**
	 * Delete the given blog video.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function destroy($id)
	{
		// Check if the blog video exists
		if (is_null($video = Video::find($id)))
		{
			// Redirect to the videos management page
			return Redirect::to('admin/videos')->with('error', Lang::get('admin/videos/message.not_found'));
		}

		// Delete the blog video
		$video->delete();

		// Redirect to the blog video management page
		return Redirect::to('admin/videos')->with('success', Lang::get('admin/videos/message.delete.success'));
	}

}
