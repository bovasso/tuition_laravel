<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Vote;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;

class VotesController extends AdminController {

	/**
	 * Show a list of all the blog vote.
	 *
	 * @return View
	 */
	public function index()
	{
		// Grab all the blog vote
		$votes = Vote::with('user.goal')->paginate(25);

		// Show the page
		return View::make('backend/votes/index', compact('votes'));
	}

	/**
	 * Blog vote update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function edit($id = null)
	{
		// Check if the blog vote exists
		if (is_null($vote = Vote::find($id)))
		{
			// Redirect to the votes management page
			return Redirect::to('admin/votes')->with('error', Lang::get('admin/votes/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/votes/edit', compact('vote'));
	}

	/**
	 * Blog vote update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function update($id = null)
	{
		// Check if the blog vote exists
		if (is_null($vote = Vote::find($id)))
		{
			// Redirect to the votes management page
			return Redirect::to('admin/votes')->with('error', Lang::get('admin/votes/message.does_not_exist'));
		}

		// Declare the rules for the form validation
		$rules = array(
			'goal_id'   => 'required|integer',
			'voted_id' => 'required|integer',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Update the blog vote data
		$vote->goal_id         = e(Input::get('goal_id'));
		$vote->voted_id          = e(Input::get('voted_id'));

		// Was the blog vote updated?
		if($vote->save())
		{
			// Redirect to the new blog vote page
			return Redirect::to("admin/votes/$id/edit")->with('success', Lang::get('admin/votes/message.update.success'));
		}

		// Redirect to the votes vote management page
		return Redirect::to("admin/votes/$id/edit")->with('error', Lang::get('admin/votes/message.update.error'));
	}

	/**
	 * Delete the given blog vote.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function destroy($id)
	{
		// Check if the blog vote exists
		if (is_null($vote = Vote::find($id)))
		{
			// Redirect to the votes management page
			return Redirect::to('admin/votes')->with('error', Lang::get('admin/votes/message.not_found'));
		}

		// Delete the blog vote
		$vote->delete();

		// Redirect to the blog vote management page
		return Redirect::to('admin/votes')->with('success', Lang::get('admin/votes/message.delete.success'));
	}

}
