<?php namespace Controllers\Admin;

use AdminController;
use Input;
use Lang;
use Goal;
use Redirect;
use Sentry;
use Str;
use Validator;
use View;

class GoalsController extends AdminController {

	/**
	 * Show a list of all the blog goal.
	 *
	 * @return View
	 */
	public function index()
	{
		// Grab all the blog goal
		$goals = Goal::compact()->orderBy('created_at', 'DESC')->paginate(10);

		// Show the page
		return View::make('backend/goals/index', compact('goals'));
	}

	/**
	 * Blog goal create.
	 *
	 * @return View
	 */
	public function create()
	{
		// Show the page
		return View::make('backend/goals/create');
	}

	/**
	 * Blog goal create form processing.
	 *
	 * @return Redirect
	 */
	public function store()
	{
		// Declare the rules for the form validation
		$rules = array(
			'headline'   => 'required|min:3',
			'body' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Create a new blog goal
		$goal = new Goal;

		// Update the blog goal data
		$goal->headline         = e(Input::get('headline'));
		$goal->body          = e(Input::get('body'));
		$goal->user_id          = Sentry::getUser()->provider_uid;

		// Was the blog goal created?
		if($goal->save())
		{
			// Redirect to the new blog goal page
			return Redirect::to("admin/goals/$goal->id/edit")->with('success', Lang::get('admin/goals/message.create.success'));
		}

		// Redirect to the blog goal create page
		return Redirect::to('admin/goals/create')->with('error', Lang::get('admin/goals/message.create.error'));
	}

	/**
	 * Blog goal update.
	 *
	 * @param  int  $id
	 * @return View
	 */
	public function edit($id = null)
	{
		// Check if the blog goal exists
		if (is_null($goal = Goal::find($id)))
		{
			// Redirect to the goals management page
			return Redirect::to('admin/goals')->with('error', Lang::get('admin/goals/message.does_not_exist'));
		}

		// Show the page
		return View::make('backend/goals/edit', compact('goal'));
	}

	/**
	 * Blog Goal update form processing page.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function update($id = null)
	{
		// Check if the blog goal exists
		if (is_null($goal = Goal::find($id)))
		{
			// Redirect to the goals management page
			return Redirect::to('admin/goals')->with('error', Lang::get('admin/goals/message.does_not_exist'));
		}

		// Declare the rules for the form validation
		$rules = array(
			'headline'   => 'required|min:3',
			'body' => 'required|min:3',
		);

		// Create a new validator instance from our validation rules
		$validator = Validator::make(Input::all(), $rules);

		// If validation fails, we'll exit the operation now.
		if ($validator->fails())
		{
			// Ooops.. something went wrong
			return Redirect::back()->withInput()->withErrors($validator);
		}

		// Update the blog goal data
		$goal->headline         = e(Input::get('headline'));
		$goal->body          = e(Input::get('body'));
		$goal->user_id          = Sentry::getUser()->provider_uid;

		// Was the blog goal updated?
		if($goal->save())
		{
			// Redirect to the new blog goal page
			return Redirect::to("admin/goals/$id/edit")->with('success', Lang::get('admin/goals/message.update.success'));
		}

		// Redirect to the goals goal management page
		return Redirect::to("admin/goals/$id/edit")->with('error', Lang::get('admin/goals/message.update.error'));
	}

	/**
	 * Delete the given blog goal.
	 *
	 * @param  int  $id
	 * @return Redirect
	 */
	public function destroy($id)
	{
		// Check if the blog goal exists
		if (is_null($goal = Goal::find($id)))
		{
			// Redirect to the goals management page
			return Redirect::to('admin/goals')->with('error', Lang::get('admin/goals/message.not_found'));
		}

		// Delete the blog goal
		$goal->delete();

		// Redirect to the blog goal management page
		return Redirect::to('admin/goals')->with('success', Lang::get('admin/goals/message.delete.success'));
	}

}
