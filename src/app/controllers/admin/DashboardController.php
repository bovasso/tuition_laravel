<?php namespace Controllers\Admin;

use AdminController;
use View;
use Goal;
use Video;

class DashboardController extends AdminController {

	/**
	 * Show the administration dashboard page.
	 *
	 * @return View
	 */
	public function index()
	{
		// Grab all the blog goal
		$goals = Goal::compact()->orderBy('created_at', 'DESC')->paginate(5);
		$videos = Video::orderBy('created_at', 'DESC')->paginate(5);


		// Show the page
		return View::make('backend/dashboard', array('goals' => $goals,'videos' => $videos));
	}

}
