<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNominationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('nominations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('nominator_id')->nullable();
			$table->bigInteger('entrant_id')->nullable();
			$table->string('entrant_email', 100)->nullable();
			$table->string('headline', 100)->nullable();
			$table->text('body')->nullable();
			$table->timestamps();
			$table->integer('is_active')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('nominations');
	}

}
