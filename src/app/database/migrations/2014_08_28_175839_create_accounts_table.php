<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('accounts', function(Blueprint $table)
		{
			$table->string('janrain_id', 60)->primary();
			$table->bigInteger('provider_uid')->nullable();
			$table->string('firstname', 20)->nullable();
			$table->string('lastname', 20)->nullable();
			$table->string('address1', 50)->nullable();
			$table->string('address2', 50)->nullable();
			$table->string('city', 20)->nullable();
			$table->char('state', 2)->nullable();
			$table->string('zip', 5)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('email', 100)->nullable();
			$table->date('birthday')->nullable();
			$table->integer('last_step_completed')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('accounts');
	}

}
