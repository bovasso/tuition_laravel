<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('entries', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('janrain_id', 60);
			$table->bigInteger('provider_uid')->index('entries_provider_uid');
			$table->string('youtube_id', 20)->nullable();
			$table->string('filename');
			$table->string('video_type', 6);
			$table->string('original_filename');
			$table->boolean('status');
			$table->boolean('game_id')->nullable()->default(0);
			$table->dateTime('date_entered');
			$table->dateTime('date_completed')->nullable();
			$table->boolean('last_step_completed')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('entries');
	}

}
