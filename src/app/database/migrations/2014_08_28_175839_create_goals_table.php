<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('goals', function(Blueprint $table)
		{
			$table->increments('id');
			$table->bigInteger('user_id')->nullable()->index('goals_user_id');
			$table->string('headline', 100)->nullable();
			$table->text('body')->nullable();
			$table->timestamps();
			$table->integer('total_votes')->nullable()->index('goals_total_votes');
			$table->boolean('status')->nullable()->default(0)->index('goals_status');
			$table->integer('is_active')->nullable()->index('goals_is_active');
			$table->boolean('leader_flag')->default(0)->index('goals_leader_flag');
			$table->integer('nomination_id')->nullable();
			$table->integer('from_nomination')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('goals');
	}

}
