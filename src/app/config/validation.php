<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| ADMIN Validation Rules
	|--------------------------------------------------------------------------
	|
	*/
	'admin' => array(
		'goal' => array(
			'headline'		=> 'required',
			'body' 			=> 'required',
			'school'		=> 'required',
			'hometown'		=> 'required',
		),
		'user' => array(),
		'entry' => array(),
	),
	/*
	|--------------------------------------------------------------------------
	| API Validation Rules
	|--------------------------------------------------------------------------
	|
	*/
	'api' => array(
		'goal' => array(
			'headline'		=> 'required',
			'body' 			=> 'required',
			'school'		=> 'required',
			'hometown'		=> 'required',
		),
		'user' => array(),
		'entry' => array(),
	),
);
