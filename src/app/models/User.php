<?php

use Cartalyst\Sentry\Users\Eloquent\User as SentryUserModel;
class User extends SentryUserModel {

	use Illuminate\Database\Eloquent\SoftDeletingTrait;

	/**
	 * Indicates if the model should soft delete.
	 *
	 * @var bool
	 */
	// protected $softDelete = true;
	protected $dates = ['deleted_at'];

	public function votes()
    {
        return $this->hasMany('Vote');
    }

    public function goal()
    {
        return $this->hasOne('Goal');
    }

    public function video()
    {
        return $this->hasOne('Video');
    }

	/**
	 * Returns the user full name, it simply concatenates
	 * the user first and last name.
	 *
	 * @return string
	 */
	public function fullName()
	{
		return "{$this->first_name} {$this->last_name}";
	}

}
