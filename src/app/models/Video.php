<?php

class Video extends \Eloquent {
	protected $fillable = [];

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];
	
	public function user(){
		return $this->belongsTo('User', 'provider_uid', 'provider_uid');
	}
}