<?php

class Vote extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function user(){
		return $this->belongsTo('User', 'voter_id', 'provider_uid');
	}

	public function story(){
		return $this->belongsTo('Goal', 'goal_id', 'id');
	}

}