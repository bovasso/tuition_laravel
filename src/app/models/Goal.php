<?php

class Goal extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		// 'title' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = [];

	public function user(){
		return $this->belongsTo('User', 'user_id', 'provider_uid');
	}

	public function votes(){
		return $this->belongsTo('User', 'voter_id', 'provider_uid');
	}

	public function scopecompact(){

		if(Sentry::check()){

			$response = DB::table('goals')
				->select(DB::raw('
				goals.headline,
				goals.body,
				goals.total_votes,
				goals.created_at,
				goals.id,
				goals.user_id,
				users.gender as gender,
				users.first_name as first_name,
				users.last_name as last_name,
				users.hometown as hometown,
				users.school as school,
				users.major as major,
				IF(votes.goal_id IS NOT NULL,TRUE,FALSE) as voted_for'))
				->groupBy('goals.id')
				->join('users', 'users.provider_uid', '=', 'goals.user_id')
				->leftJoin('votes', function($join){
					$join->on('votes.goal_id', '=', 'goals.id')
					->on('votes.voter_id', '=', DB::raw(Sentry::getUser()->provider_uid));
	        });

		} else {

			$response = DB::table('goals')
				->select(DB::raw('
				goals.headline,
				goals.body,
				goals.total_votes,
				goals.created_at,
				goals.id,
				goals.user_id,
				users.gender as gender,
				users.first_name as first_name,
				users.last_name as last_name,
				users.hometown as hometown,
				users.school as school,
				users.major as major'))
				->groupBy('goals.id')
				->join('users', 'users.provider_uid', '=', 'goals.user_id')
				->leftJoin('votes', function($join){
						$join->on('votes.goal_id', '=', 'goals.id')
				;		        
			});			
		}



		return $response;
	}

}