<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/

Route::group(array('prefix' => 'admin'), function()
{	
	# Videos Management
	Route::group(array('prefix' => 'videos'), function()
	{
		Route::get('/', array('as' => 'videos', 'uses' => 'Controllers\Admin\VideosController@index'));
		Route::get('{id}/destroy', array('as' => 'videos/destroy', 'uses' => 'Controllers\Admin\VideosController@destroy'));
		Route::get('{id}/edit', array('uses' => 'Controllers\Admin\VideosController@edit'));
		Route::get('{id}/show', array('uses' => 'Controllers\Admin\VideosController@show'));
	});	

	# Goals Management
	Route::group(array('prefix' => 'votes'), function()
	{
		Route::get('/', array('as' => 'votes', 'uses' => 'Controllers\Admin\VotesController@index'));
		Route::get('{id}/destroy', array('as' => 'goals/destroy', 'uses' => 'Controllers\Admin\VotesController@destroy'));
		Route::get('{id}/edit', array('uses' => 'Controllers\Admin\VotesController@edit'));
		Route::get('{id}/show', array('uses' => 'Controllers\Admin\VotesController@show'));
	});	

	# Goals Management
	Route::group(array('prefix' => 'goals'), function()
	{
		Route::get('/', array('as' => 'goals', 'uses' => 'Controllers\Admin\GoalsController@index'));
		Route::get('{id}/destroy', array('as' => 'goals/destroy', 'uses' => 'Controllers\Admin\GoalsController@destroy'));
		Route::get('{id}/edit', array('uses' => 'Controllers\Admin\GoalsController@edit'));
		Route::get('{id}/show', array('uses' => 'Controllers\Admin\GoalsController@show'));
	});	

	# User Management
	Route::group(array('prefix' => 'users'), function()
	{
		Route::get('/', array('as' => 'users', 'uses' => 'Controllers\Admin\UsersController@getIndex'));
		Route::get('create', array('as' => 'create/user', 'uses' => 'Controllers\Admin\UsersController@getCreate'));
		Route::post('create', 'Controllers\Admin\UsersController@postCreate');
		Route::get('{userId}/edit', array('as' => 'update/user', 'uses' => 'Controllers\Admin\UsersController@getEdit'));
		Route::post('{userId}/edit', 'Controllers\Admin\UsersController@postEdit');
		Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'Controllers\Admin\UsersController@getDelete'));
		Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'Controllers\Admin\UsersController@getRestore'));
	});

	# Group Management
	Route::group(array('prefix' => 'groups'), function()
	{
		Route::get('/', array('as' => 'groups', 'uses' => 'Controllers\Admin\GroupsController@getIndex'));
		Route::get('create', array('as' => 'create/group', 'uses' => 'Controllers\Admin\GroupsController@getCreate'));
		Route::post('create', 'Controllers\Admin\GroupsController@postCreate');
		Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Controllers\Admin\GroupsController@getEdit'));
		Route::post('{groupId}/edit', 'Controllers\Admin\GroupsController@postEdit');
		Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Controllers\Admin\GroupsController@getDelete'));
		Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Controllers\Admin\GroupsController@getRestore'));
	});

	Route::get('/', array('as' => 'admin', 'uses' => 'Controllers\Admin\DashboardController@index'));
	
	Route::resource('goals', 'Controllers\Admin\GoalsController');
	Route::resource('votes', 'Controllers\Admin\VotesController');
	Route::resource('videos', 'Controllers\Admin\VideosController');


});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/

Route::group(array('prefix' => 'api/v1'), function()
{
	// header('Access-Control-Allow-Origin: http://tuition.com:3000');
	header('Access-Control-Allow-Origin: *');

	Route::group(array('prefix' => 'goals'), function()
	{
		Route::resource('goals', 'Controllers\Api\GoalsController');
		Route::get('/', array('as' => 'home', 'uses' => 'Controllers\Api\GoalsController@index'));
		Route::get('/leaderboard', array('as' => 'leaderboard', 'uses' => 'Controllers\Api\GoalsController@leaderboard'));
		Route::get('/{user_id}', array('uses' => 'Controllers\Api\GoalsController@index'));
	});
});

/*
|--------------------------------------------------------------------------
| Authentication and Authorization Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'auth'), function()
{

	# Login
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');

	# Register
	Route::get('signup', array('as' => 'signup', 'uses' => 'AuthController@getSignup'));
	Route::post('signup', 'AuthController@postSignup');

	# Account Activation
	Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

	# Forgot Password
	Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword'));
	Route::post('forgot-password', 'AuthController@postForgotPassword');

	# Forgot Password Confirmation
	Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

});

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'account'), function()
{

	# Account Dashboard
	Route::get('/', array('as' => 'account', 'uses' => 'Controllers\Account\DashboardController@getIndex'));

	# Profile
	Route::get('profile', array('as' => 'profile', 'uses' => 'Controllers\Account\ProfileController@getIndex'));
	Route::post('profile', 'Controllers\Account\ProfileController@postIndex');

	# Change Password
	Route::get('change-password', array('as' => 'change-password', 'uses' => 'Controllers\Account\ChangePasswordController@getIndex'));
	Route::post('change-password', 'Controllers\Account\ChangePasswordController@postIndex');

	# Change Email
	Route::get('change-email', array('as' => 'change-email', 'uses' => 'Controllers\Account\ChangeEmailController@getIndex'));
	Route::post('change-email', 'Controllers\Account\ChangeEmailController@postIndex');

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('about-us', function()
{
	//
	return View::make('frontend/about-us');
});

Route::get('contact-us', array('as' => 'contact-us', 'uses' => 'ContactUsController@getIndex'));
Route::post('contact-us', 'ContactUsController@postIndex');

// Route::get('blog/{postSlug}', array('as' => 'view-post', 'uses' => 'BlogController@getView'));
// Route::post('blog/{postSlug}', 'BlogController@postView');

Route::resource('goals', 'GoalsController');
Route::get('/', array('as' => 'home', 'uses' => 'GoalsController@index'));
