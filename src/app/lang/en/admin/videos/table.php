<?php

return array(

	'headline'      => 'Headline',
	'body'   => 'Body',
	'fb' => 'FB UID',
	'school' => 'School',
	'hometown' => 'Hometown',
	'name' => 'Name',
	'voter' => 'Voter\'s Name',
	'story' => 'Story Owner', 
	'youtube' => 'Youtube Link', 
	'created_at' => 'Created at',

);
