<?php

return array(

	'does_not_exist' => 'Video does not exist.',

	'create' => array(
		'error'   => 'Video was not created, please try again.',
		'success' => 'Video created successfully.'
	),

	'update' => array(
		'error'   => 'Video was not updated, please try again',
		'success' => 'Video updated successfully.'
	),

	'delete' => array(
		'error'   => 'There was an issue deleting the video. Please try again.',
		'success' => 'The video was deleted successfully.'
	)

);
