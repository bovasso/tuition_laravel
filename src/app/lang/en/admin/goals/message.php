<?php

return array(

	'does_not_exist' => 'Goal does not exist.',

	'create' => array(
		'error'   => 'Goal was not created, please try again.',
		'success' => 'Goal created successfully.'
	),

	'update' => array(
		'error'   => 'Goal was not updated, please try again',
		'success' => 'Goal updated successfully.'
	),

	'delete' => array(
		'error'   => 'There was an issue deleting the goal. Please try again.',
		'success' => 'The goal was deleted successfully.'
	)

);
