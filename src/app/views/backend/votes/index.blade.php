@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Vote Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><i class="fa fa-align-justify"></i><span class="break"></span>votes</h2>
		<div class="panel-actions">
			<a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
			<a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
			<a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
<table class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th class="span6">ID</th>
			<th class="span2">@lang('admin/votes/table.voter')</th>
			<th class="span2">@lang('admin/votes/table.goal')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($votes as $vote)
		<tr>
			<td><a href="{{ URL::to('admin/votes/'. $vote->id .'/edit') }}">{{ $vote->id }}</a></td>
			<td><a href="{{ URL::to('admin/users/'.$vote->user['id'].'/edit') }}">{{ $vote->user['first_name'] .' '. $vote->user['last_name'] }}</a></td>
			<td>{{ $vote->created_at }}</td>
			<td>
				<a class="btn btn-success" href="{{ URL::to('admin/votes/'.$vote->id.'/show') }}">
				<i class="fa fa-search-plus "></i>  
				</a>
				<a class="btn btn-info" href="{{ URL::to('admin/votes/'.$vote->id.'/edit') }}">
				<i class="fa fa-edit "></i>  
				</a>
				<a class="btn btn-danger" href="{{ URL::to('admin/votes/'.$vote->id.'/destroy') }}">
				<i class="fa fa-trash-o "></i> 
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $votes->links() }}
</div>
</div>
@stop
