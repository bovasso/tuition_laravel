@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Vote Update ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><strong>Vote</strong> Edit</h2>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" method="post" action="" autocomplete="off">
			<!-- CSRF Token -->
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="form-group">
				<label for="headline">Headline</label>
				<input type="text" id="headline" name="headline" class="form-control" value="{{ Input::old('headline', $goal->headline) }}">
				<span class="help-block">Please enter headline</span>
				{{ $errors->first('headline', '<span class="help-inline">:message</span>') }}
			</div>
			<div class="form-group">
				<label class="control-label" for="textarea2">Body</label>
				<div class="controls">
					<textarea id="body" name="body" rows="9" class="form-control" placeholder="Body.." style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 194px;">{{ Input::old('body', $goal->body) }}</textarea>
				</div>
			</div>
		</form>
	</div>
	<div class="panel-footer">
		<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-dot-circle-o"></i> Submit</button>
		<button type="reset" class="btn btn-sm btn-danger"><i class="fa fa-ban"></i> Reset</button>
		<a href="{{ route('goals') }}" class="btn btn-sm btn-inverse"><i class="fa fa-ban"></i> Back</a>
	</div>
@stop
