@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Video Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><i class="fa fa-align-justify"></i><span class="break"></span>Videos</h2>
		<div class="panel-actions">
			<a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
			<a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
			<a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
<table class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th class="span6">ID</th>
			<th class="span2">@lang('admin/videos/table.youtube')</th>
			<th class="span2">@lang('admin/videos/table.name')</th>
			<th class="span2">@lang('admin/videos/table.created_at')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($videos as $video)
		<tr>
			<td><a href="{{ URL::to('admin/videos/'. $video->id .'/edit') }}">{{ $video->id }}</a></td>
			<td><a href="{{ URL::to('https://www.youtube.com/watch?v=' . $video->youtube_id) }}" target="_blank">{{ $video->youtube_id }}</a></td>
			<td><a href="{{ URL::to('admin/users/'.$video->user['id'].'/edit') }}">{{ $video->user['first_name'] .' '. $video->user['last_name'] }}</a></td>
			<td>{{ $video->created_at }}</td>
			<td>
				<a class="btn btn-success" href="{{ URL::to('admin/videos/'.$video->id.'/show') }}">
				<i class="fa fa-search-plus "></i>  
				</a>
				<a class="btn btn-info" href="{{ URL::to('admin/videos/'.$video->id.'/edit') }}">
				<i class="fa fa-edit "></i>  
				</a>
				<a class="btn btn-danger" href="{{ URL::to('admin/videos/'.$video->id.'/destroy') }}">
				<i class="fa fa-trash-o "></i> 
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $videos->links() }}
</div>
</div>
@stop
