<!DOCTYPE html>
<html lang="en">
	<head>
    	<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- -->
		<link rel="shortcut icon" href="/favicon.ico">

	    <title>Tuition CMS</title>

	    <!-- Bootstrap core CSS -->
	    <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet">
		
		<!-- page css files -->
		<link href="{{asset('assets/admin/css/font-awesome.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/admin/css/jquery-ui.min.css')}}" rel="stylesheet">
		<link href="{{asset('assets/admin/css/fullcalendar.css')}}" rel="stylesheet">
		<link href="{{asset('assets/admin/css/morris.css')}}" rel="stylesheet">
		<link href="{{asset('assets/admin/css/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet">
		<link href="{{asset('assets/admin/css/climacons-font.css')}}" rel="stylesheet">

	    <!-- Custom styles for this template -->
	    <link href="{{asset('assets/admin/css/style.min.css')}}" rel="stylesheet">

	    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')}}"></script>
	    <![endif]-->
	</head>
</head>

<body>
	@include('backend.partials._header')	
	<div class="container-fluid content">
		<div class="row">
			@include('backend.partials._sidenav')	
			<!-- start: Content -->
			<div class="col-md-10 col-sm-11 main ">
				<div class="row">
					<!-- Notifications -->
					@include('frontend/notifications')
					<!-- Content -->
					@yield('content')
				</div><!--/row-->
			</div>		
		</div>
	</div><!--/container-->
		
	
	<div class="modal fade" id="myModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<p>Here settings can be configured...</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	
	<div class="clearfix"></div>
		
	<!-- start: JavaScript-->
	<!--[if !IE]>-->

			<script src="{{asset('assets/admin/js/jquery-2.1.0.min.js')}}"></script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script src="{{asset('assets/admin/js/jquery-1.11.0.min.js')}}"></script>
	
	<![endif]-->

	<!--[if !IE]>-->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='{{asset('assets/admin/js/jquery-2.1.0.min.js')}}'>"+"<"+"/script>");
		</script>

	<!--<![endif]-->

	<!--[if IE]>
	
		<script type="text/javascript">
	 	window.jQuery || document.write("<script src='/admin/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
		</script>
		
	<![endif]-->
	<script src="{{asset('assets/admin/js/jquery-migrate-1.2.1.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/bootstrap.min.js')}}"></script>	
	
	
	<!-- page scripts -->
	<script src="{{asset('assets/admin/js/jquery-ui.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery.ui.touch-punch.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery.sparkline.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/fullcalendar.min.js')}}"></script>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="{{asset('assets/admin/js/excanvas.min.js')}}"></script><![endif]-->
	<script src="{{asset('assets/admin/js/jquery.autosize.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery.placeholder.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/moment.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/daterangepicker.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery.easy-pie-chart.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/dataTables.bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/raphael.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/morris.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/uncompressed/jquery-jvectormap-world-mill-en.js')}}"></script>
	<script src="{{asset('assets/admin/js/uncompressed/gdp-data.js')}}"></script>
	<script src="{{asset('assets/admin/js/gauge.min.js')}}"></script>
	
	<!-- theme scripts -->
	<script src="{{asset('assets/admin/js/custom.min.js')}}"></script>
	<script src="{{asset('assets/admin/js/core.min.js')}}"></script>
	
	<!-- inline scripts related to this page -->
	
	<!-- end: JavaScript-->
	
</body>
</html>