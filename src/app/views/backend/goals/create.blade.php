@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Create a New Goal ::
@parent
@stop

{{-- Page body --}}
@section('content')
<div class="page-header">
	<h3>
		Create a New Goal

		<div class="pull-right">
			<a href="{{ route('goals') }}" class="btn btn-small btn-inverse"><i class="icon-circle-arrow-left icon-white"></i> Back</a>
		</div>
	</h3>
</div>

<!-- Tabs -->
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">General</a></li>
	<li><a href="#tab-meta-data" data-toggle="tab">Meta Data</a></li>
</ul>

<form class="form-horizontal" method="post" action="" autocomplete="off">
	<!-- CSRF Token -->
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />

	<!-- Tabs body -->
	<div class="tab-content">
		<!-- General tab -->
		<div class="tab-pane active" id="tab-general">
			<!-- Post headline -->
			<div class="control-group {{ $errors->has('headline') ? 'error' : '' }}">
				<label class="control-label" for="headline">Goal headline</label>
				<div class="controls">
					<input class="span10" type="text" name="headline" id="headline" value="{{ Input::old('headline') }}" />
					{{ $errors->first('headline', '<span class="help-inline">:message</span>') }}
				</div>
			</div>

			<!-- Post Slug -->
			<div class="control-group">
				<label class="control-label" for="slug">Slug</label>
				<div class="controls">
					<div class="input-prepend">
						<span class="add-on">
							{{ str_finish(URL::to('/'), '/') }}
						</span>
						<input class="span6" type="text" name="slug" id="slug" value="{{ Input::old('slug') }}">
					</div>
				</div>
			</div>

			<!-- body -->
			<div class="control-group {{ $errors->has('body') ? 'error' : '' }}">
				<label class="control-label" for="body">body</label>
				<div class="controls">
					<textarea class="span10" name="body" id="body" value="body" rows="10">{{ Input::old('body') }}</textarea>
					{{ $errors->first('body', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
		</div>

		<!-- Meta Data tab -->
		<div class="tab-pane" id="tab-meta-data">
			<!-- Meta headline -->
			<div class="control-group {{ $errors->has('meta-headline') ? 'error' : '' }}">
				<label class="control-label" for="meta-headline">Meta headline</label>
				<div class="controls">
					<input class="span10" type="text" name="meta-headline" id="meta-headline" value="{{ Input::old('meta-headline') }}" />
					{{ $errors->first('meta-headline', '<span class="help-inline">:message</span>') }}
				</div>
			</div>

			<!-- Meta Description -->
			<div class="control-group {{ $errors->has('meta-description') ? 'error' : '' }}">
				<label class="control-label" for="meta-description">Meta Description</label>
				<div class="controls">
					<input class="span10" type="text" name="meta-description" id="meta-description" value="{{ Input::old('meta-description') }}" />
					{{ $errors->first('meta-description', '<span class="help-inline">:message</span>') }}
				</div>
			</div>

			<!-- Meta Keywords -->
			<div class="control-group {{ $errors->has('meta-keywords') ? 'error' : '' }}">
				<label class="control-label" for="meta-keywords">Meta Keywords</label>
				<div class="controls">
					<input class="span10" type="text" name="meta-keywords" id="meta-keywords" value="{{ Input::old('meta-keywords') }}" />
					{{ $errors->first('meta-keywords', '<span class="help-inline">:message</span>') }}
				</div>
			</div>
		</div>
	</div>

	<!-- Form actions -->
	<div class="control-group">
		<div class="controls">
			<a class="btn btn-link" href="{{ route('blogs') }}">Cancel</a>

			<button type="reset" class="btn">Reset</button>

			<button type="submit" class="btn btn-success">Publish</button>
		</div>
	</div>
</form>
@stop
