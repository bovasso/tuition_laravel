@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Goal Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><i class="fa fa-align-justify"></i><span class="break"></span>Goals</h2>
		<div class="panel-actions">
			<a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
			<a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
			<a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
<table class="table table-striped table-bordered bootstrap-datatable datatable">
	<thead>
		<tr>
			<th class="span6">@lang('admin/goals/table.fb')</th>
			<th class="span2">@lang('admin/goals/table.name')</th>
			<th class="span2">@lang('admin/goals/table.school')</th>
			<th class="span2">@lang('admin/goals/table.hometown')</th>
			<th class="span2">@lang('admin/goals/table.created_at')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($goals as $key => $goal)
		<tr>
			<td><a href="{{ URL::to('admin/goals/'.$goal->id.'/edit') }}">{{ $goal->user_id }}</a></td>
			<td>{{ $goal->first_name." ".$goal->last_name }}</td>
			<td>{{ $goal->school }}</td>
			<td>{{ $goal->hometown }}</td>
			<td>{{ $goal->created_at }}</td>
			<td>
				<a class="btn btn-success" href="{{ URL::to('admin/goals/'.$goal->id.'/show') }}">
				<i class="fa fa-search-plus "></i>  
				</a>
				<a class="btn btn-info" href="{{ URL::to('admin/goals/'.$goal->id.'/edit') }}">
				<i class="fa fa-edit "></i>  
				</a>
				<a class="btn btn-danger" href="{{ URL::to('admin/goals/'.$goal->id.'/destroy') }}">
				<i class="fa fa-trash-o "></i> 
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $goals->links() }}
</div>
</div>
@stop
