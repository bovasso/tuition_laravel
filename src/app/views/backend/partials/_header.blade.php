	<!-- start: Header -->
<div class="navbar" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar">a</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><img src="/assets/admin/img/logo.png" width="40px" height="30px" /></a>
        </div>
		<ul class="nav navbar-nav navbar-actions navbar-left">
			<li><a href="#" id="main-menu-toggle"><i class="fa fa-bars"></i></a></li>
		</ul>
        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-comments-o"></i><span class="badge">4</span></a>
        		<ul class="dropdown-menu">
					<li class="dropdown-menu-header">
						<strong>Messages</strong>
						<div class="progress thin">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
						    <span class="sr-only">40% Complete (success)</span>
						  </div>
						</div>
					</li>
					<li class="avatar">
						<a href="#">
							<img src="/admin/img/avatar.jpg">
							<div>New message</div>
							<small>1 minute ago</small>
							<span class="label label-info">NEW</span>
						</a>
					</li>	
        		</ul>
      		</li>
			<li class="dropdown">
        		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i><span class="badge">9</span></a>
        		<ul class="dropdown-menu">
					<li class="dropdown-menu-header text-center">
						<strong>Settings</strong>
					</li>
					<li{{ Request::is('account/profile') ? ' class="active"' : '' }}><a href="{{ URL::route('profile') }}"><i class="fa fa-user"></i> Profile</a></li>
					<li{{ Request::is('account/change-password') ? ' class="active"' : '' }}><a href="{{ URL::route('change-password') }}"><i class="fa fa-user"></i> Change Password</a></li>
					<li{{ Request::is('account/change-email') ? ' class="active"' : '' }}><a href="{{ URL::route('change-email') }}"><i class="fa fa-user"></i> Change Email</a></li>
					<li class="divider"></li>
					<li><a href="{{ route('logout') }}"><i class="fa fa-lock"></i> Logout</a></li>	
        		</ul>
      		</li>
		</ul>
		<form class="navbar-form navbar-left">
			<i class="fa fa-search"></i>
			<input type="text" class="form-control" placeholder="Search...">
		</form>
	</div>
</div>
<!-- end: Header -->