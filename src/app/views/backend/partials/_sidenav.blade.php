<!-- start: Main Menu -->
<div class="sidebar col-md-2 col-sm-1 ">
					
	<div class="sidebar-collapse collapse">
		<div class="nav-sidebar title"><span>Main</span></div>
		<ul class="nav nav-sidebar">
			<li {{ (Request::is('admin/dashboard*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin') }}"><i class="fa fa-dashboard"></i><span class="hidden-sm text"> Dashboard</span></a></li>
		</ul>
		<div class="nav-sidebar title"><span>Content</span></div>
		<ul class="nav nav-sidebar">
			<li{{ (Request::is('admin/goals*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/goals') }}"><i class="fa fa-star"></i><span class="hidden-sm text"> Goals</span></a></li>
			<li{{ (Request::is('admin/videos*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/videos') }}"><i class="fa fa-video-camera"></i><span class="hidden-sm text"> Videos</span></a></li>
			<li{{ (Request::is('admin/votes*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/votes') }}"><i class="fa fa-thumbs-up"></i><span class="hidden-sm text"> Votes</span></a></li>	
		</ul>

		<div class="nav-sidebar title"><span>Accounts</span></div>
			<ul class="nav nav-sidebar">
				<li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a class="submenu" href="{{ URL::to('admin/users') }}"><i class="fa fa-user"></i><span class="hidden-sm text"> Users</span></a></li>
				<li{{ (Request::is('admin/groups*') ? ' class="active"' : '') }}><a class="submenu" href="{{ URL::to('admin/groups') }}"><i class="fa fa-users"></i><span class="hidden-sm text"> Groups</span></a></li>
			</ul>
	
		<a href="#" id="main-menu-min" class="full visible-md visible-lg"><i class="fa fa-angle-double-left"></i></a>
	</div>

</div>
<!-- end: Main Menu -->