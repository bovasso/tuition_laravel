@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Goal Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="pull-right">
	<a href="{{ route('create/user') }}" class="btn btn-small btn-info"><i class="icon-plus-sign icon-white"></i> Create New User</a>
</div>
<div class="clearfix"></div>
<br />
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><i class="fa fa-align-justify"></i><span class="break"></span>User Groups</h2>
		<div class="panel-actions">
			<a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
			<a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
			<a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
	<a class="btn btn-sm btn-inverse" href="{{ URL::to('admin/users?withTrashed=true') }}">Include Deleted Users</a>
<a class="btn btn-sm btn-danger" href="{{ URL::to('admin/users?onlyTrashed=true') }}">Include Only Deleted Users</a>
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="span1">@lang('admin/users/table.id')</th>
			<th class="span2">@lang('admin/users/table.first_name')</th>
			<th class="span2">@lang('admin/users/table.last_name')</th>
			<th class="span3">@lang('admin/users/table.email')</th>
			<th class="span2">@lang('admin/users/table.activated')</th>
			<th class="span2">@lang('admin/users/table.created_at')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->id }}</td>
			<td>{{ $user->first_name }}</td>
			<td>{{ $user->last_name }}</td>
			<td>{{ $user->email }}</td>
			<td>@lang('general.' . ($user->isActivated() ? 'yes' : 'no'))</td>
			<td>{{ $user->created_at->diffForHumans() }}</td>
			<td>
				<a href="{{ route('update/user', $user->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit "></i></a>

				@if ( ! is_null($user->deleted_at))
				<a href="{{ route('restore/user', $user->id) }}" class="btn btn-sm btn-warning">@lang('button.restore')</a>
				@else
				@if (Sentry::getId() !== $user->id)
				<a href="{{ route('delete/user', $user->id) }}" class="btn btn-sm btn-danger">
					<i class="fa fa-trash-o "></i> 
				</a>
				@else
				<span class="btn btn-sm btn-danger disabled">
					<i class="fa fa-trash-o "></i> 
				</span>
				@endif
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
{{ $users->links() }}
</div>

</div>

@stop


