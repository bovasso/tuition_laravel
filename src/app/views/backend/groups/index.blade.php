@extends('backend/layouts/admin')

{{-- Page title --}}
@section('title')
Goal Management ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="pull-right">
	<a href="{{ route('create/group') }}" class="btn btn-sm btn-info"><i class="icon-plus-sign icon-white"></i> Create New Group</a>
</div>
<div class="clearfix"></div>
<br />
<div class="panel panel-default">
	<div class="panel-heading">
		<h2><i class="fa fa-align-justify"></i><span class="break"></span>User Groups</h2>
		<div class="panel-actions">
			<a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
			<a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
			<a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
		</div>
	</div>
	<div class="panel-body">
<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th class="span1">@lang('admin/groups/table.id')</th>
			<th class="span6">@lang('admin/groups/table.name')</th>
			<th class="span2">@lang('admin/groups/table.users')</th>
			<th class="span2">@lang('admin/groups/table.created_at')</th>
			<th class="span2">@lang('table.actions')</th>
		</tr>
	</thead>
	<tbody>
		@if ($groups->count() >= 1)
		@foreach ($groups as $group)
		<tr>
			<td>{{ $group->id }}</td>
			<td>{{ $group->name }}</td>
			<td>{{ $group->users()->count() }}</td>
			<td>{{ $group->created_at->diffForHumans() }}</td>
			<td>
				<a href="{{ route('update/group', $group->id) }}" class="btn btn-mini">@lang('button.edit')</a>
				<a href="{{ route('delete/group', $group->id) }}" class="btn btn-mini btn-danger">@lang('button.delete')</a>
			</td>
		</tr>
		@endforeach
		@else
		<tr>
			<td colspan="5">No results</td>
		</tr>
		@endif
	</tbody>
</table>
{{ $groups->links() }}
</div>

</div>

@stop

