@extends('frontend/layouts/default')

@section('title')
<title>Dr Pepper Tuition Giveaway</title>
@endsection

{{-- Page content --}}
@section('content')

	<div class="row">
		@foreach($goals as $goal)
		<div class="col-md-4">
			<h2>{{$goal->headline}}</h2>
			<p>{{$goal->body}} </p>
			<p><a class="btn btn-default" href="{{URL::to('goals/'. $goal->user_id)}}" role="button">View details &raquo;</a></p>
		</div>
		@endforeach
		{{$goals->links()}}
	</div>


@endsection

{{-- Page specific meta and og tags --}}
@section('meta')
@endsection

{{-- Page specific css --}}
@section('css')
@endsection

{{-- Page specific js --}}
@section('scripts')
@endsection
