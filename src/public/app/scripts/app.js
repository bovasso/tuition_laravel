'use strict';

angular.module('tuitionApp', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.bootstrap', 'ui.router', 'facebook'])
	.config(function($stateProvider, $urlRouterProvider, RestangularProvider, FacebookProvider) {
		
		// init FB App
		FacebookProvider.init('794500927238783');

		// Setup Base URL for Server-Side API
		RestangularProvider.setBaseUrl('http://api.tuition.dev/api/v1');
		// Adjust response data index based on whether performing List vs Show
		RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
			var extractedData;
			if (operation === "getList") {
				extractedData = data.data;
				extractedData.per_page = data.per_page;
				extractedData.current_page = data.current_page;
				extractedData.from = data.from;
				extractedData.last_page = data.last_page;
				extractedData.to = data.to;
				extractedData.total = data.total;
			} else {
				extractedData = data;
			}
			return extractedData;
		});

		// Setup Routes
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: 'partials/goals/list.html',
				controller: 'GoalsCtrl'
			})
			.state('leaderboard', {
				url: '/leaderboard',
				templateUrl: 'partials/goals/leaderboard.html',
				controller: 'GoalsCtrl'
			})
			.state('goal', {
				url: '/goals/:id',
				templateUrl: 'partials/goals/show.html',
				controller: 'GoalsCtrl'
			});

		$urlRouterProvider.otherwise('/');

	});