'use strict';

/**
 * @ngdoc function
 * @name tuitionAppApp.controller:GoalsCtrl
 * @description
 * # GoalsCtrl
 * Controller of the tuitionApp
 */
angular.module('tuitionApp')
	.controller('GoalsCtrl', function($http, $scope, $stateParams, Restangular) {
		var id = $stateParams.id;  
		$scope.find = function() {
			var goals = Restangular.all('goals');
			Restangular.all('goals').getList()  // GET: /goals
			.then(function(goals) {
				// returns a list of goals
				$scope.goals = goals; 
			});
		};
		$scope.findOne = function() {
			Restangular.one('goals', id).get()  // GET: /goals
			.then(function(goals) {
				// returns a list of goals
				$scope.goal = goals[0]; 
			});
		};
		$scope.leaderboard = function() {
			Restangular.all('goals/leaderboard').getList('leaderboard')  // GET: /goals
			.then(function(goals) {
				// returns a list of goals
				$scope.goals = goals; 
			});
		};
	});
