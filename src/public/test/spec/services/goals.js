'use strict';

describe('Service: goals', function () {

  // load the service's module
  beforeEach(module('tuitionAppApp'));

  // instantiate service
  var goals;
  beforeEach(inject(function (_goals_) {
    goals = _goals_;
  }));

  it('should do something', function () {
    expect(!!goals).toBe(true);
  });

});
